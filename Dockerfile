FROM nginx:latest

COPY ./html/index.html /usr/share/nginx/html/index.html
COPY ./html/site-aaa /usr/share/nginx/html/site-aaa
COPY ./html/site-bbb /usr/share/nginx/html/site-bbb
COPY ./html/site-ccc /usr/share/nginx/html/site-ccc

COPY ./sites-available/default  /etc/nginx/sites-available/
COPY ./sites-available/site-aaa /etc/nginx/sites-available/
COPY ./sites-available/site-bbb /etc/nginx/sites-available/
COPY ./sites-available/site-ccc /etc/nginx/sites-available/

COPY nginx.conf /etc/nginx/

RUN mkdir /etc/nginx/sites-enabled
RUN ln -s /etc/nginx/sites-available/default  /etc/nginx/sites-enabled/
RUN ln -s /etc/nginx/sites-available/site-aaa /etc/nginx/sites-enabled/
RUN ln -s /etc/nginx/sites-available/site-bbb /etc/nginx/sites-enabled/
RUN ln -s /etc/nginx/sites-available/site-ccc /etc/nginx/sites-enabled/


