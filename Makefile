LOCAL_WWW=~/src/docker/nginx/html/bigmarv

# nginx's port in the container
CPORT?=80

# What port to expose for nginx outside the conainer
EPORT?=9000

# This is the name of the container we'll instantiate
CONTAINER=nginx

# This is the name of the package we'll build from our Dockerfile
PACKAGE=webserver

# The command to invoke Docker (on my system, we need to invoke as root)
CMD=sudo docker


.PHONY: start
start: build
	$(CMD) run --name $(CONTAINER) -d -p $(EPORT):$(CPORT) $(PACKAGE)
 
build: Dockerfile
	$(CMD) build -t $(PACKAGE) .

.PHONY: stop
stop:
	$(CMD) stop $(CONTAINER)
	$(CMD) rm $(CONTAINER)

.PHONY: rm
rm:
	$(CMD) rm $(CONTAINER)
 
.PHONY: logs
logs:
	$(CMD) container logs $(CONTAINER)
 
.PHONY: login
login:
	$(CMD) exec -it $(CONTAINER) bash
 
.PHONY: status
status:
	$(CMD) ps -f name=$(CONTAINER)

.PHONY: test
test:
	######## Test aaa.local ########
	curl aaa.local:$(EPORT)
	######## Test bbb.local ########
	curl bbb.local:$(EPORT)
	######## Test ccc.local ########
	curl ccc.local:$(EPORT)
	######## Test the default configuration ########
	curl localhost:$(EPORT)

