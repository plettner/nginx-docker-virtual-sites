# nginx-docker-virtual-sites

This project is an example of how to run nginx webserver
in Docker and serving up multiple virtual sites.

I have multiple domain names.  I want to run one instance
of a web server to host those domains.  (There are other
approaches but this is my first pass at the problem.)

For this example, I set up three "dummy" domains:

* aaa.local
* bbb.local
* ccc.local

The project will set up virtual sites for each, and
provide a directory for each site to hold the content.
The Docker container will be configured with everything
it needs to host these virtual domains.  What it WON'T 
have are entries in your hosts file for the three fake
domains.

When this Docker container is deployed and running, you
will be able to use your web browser (or `curl`) to view
the three domains as hosted on the `nginx` server.

## Getting the Project

This project is hosted here on my GitLab account.  To 
get it on your computer, use the `git clone` command:

```
git clone https://gitlab.com/plettner/nginx-docker-virtual-sites.git
```

Once you've done this, cd into the directory that was just created.

```
cd nginx-docker-virtual-sites
```

## Preparing to Run the Project

The one bit of preparation you'll need to do is to set up three
domain names in your machine's host file.  On *nix systems (like
Ubuntu or Mac OSX) this file is `/etc/hosts`.  Add the following 
three lines to your hosts file:

```
127.0.0.1   aaa.local
127.0.0.1   bbb.local
127.0.0.1   ccc.local
```

On the Mac or Linux, you'll likely need to use sudo to edit this
file.  You can use `vi`

```
sudo vi /etc/hosts
```

or `nano`

```
sudo nano /etc/hosts
```

## Starting the Container

I set up a Makefile that encapsulates all the Docker commands
you'll need to start, run, and stop this container.  To start
the nginx container, type

```
make
```

or

```
make start
```

(This assumes a *nix system with Gnu Make on it.)  The Makefile
is set up to default to `make start`, which builds the Dockerfile
that configures nginx with the virtual hosts, and then starts it
running.

The server is configured by default to use port 9000.  You can
change this easily in the Makefile.

Once it's running, you can test it with commands like:

```
curl aaa.local:9000
curl bbb.local:9000
curl ccc.local:9000
curl localhost:9000
```

Alternatively, you can simply type `make test` and the Makefile
will run all of these curl commands for you, saving you some 
typing.

The first three will show you the HTML for the three virtual 
domains.  When you load localhost, it'll load the default web
page that will provide links to the virtual domains.

You can also load these pages in your web browser.  Be sure
to prefix the domain name with `http://` so that your browser
will recognize the domain as a URL and not something to search 
for.  I've provided some handy links here; make sure the 
container is running first:

* [http://aaa.local:9000/](http://aaa.local:9000/)
* [http://bbb.local:9000/](http://bbb.local:9000/)
* [http://ccc.local:9000/](http://ccc.local:9000/)
* [http://localhost:9000/](http://localhost:9000/)

When you're done experimenting with running the container, shut 
it down.  Again, there's a command for this in the Makefile:

```
make stop
```

This command will stop the container and then remove it from Docker.

## Inspecting the Container

You can "log into" the container while it's running and browse
around to see how things are configured.  The command is, again,
in the Makefile.  Type:

```
make login
```

## Areas of Interest

Areas of interest in the running container for this project are:

```
/etc/nginx/nginx.conf
/etc/nginx/sites-available
/etc/nginx/sites-enabled
/usr/shared/nginx/html
/usr/shared/nginx/html/site-aaa
/usr/shared/nginx/html/site-bbb
/usr/shared/nginx/html/site-ccc
```

## Cleaning Up

Don't forget to shut down the container when you're done experimenting.

Don't forget to remove the bogus sites from your hosts file.


## Notes

### nginx configuration

As a newbie to nginx, I struggled a bit with the virtual host config files.
Pay particular attention to which paths require an ending slash (/) and 
which lines require a terminating semi-colon (;).  If you make some changes
and things don't work, use `make login` to connect to your container.  Then
type `nginx -t` (no sudo required) to see if your nginx configuration has 
any errors.  Be sure that you're running `nginx -t` in the conainer and not
on your local machine.

### Website Content

The way I have things configured, the three virtual domains are directories
inside the default domain.  This means that someone could navigate from
the default domain into the other three domains without visiting their URL.
Whether this is behavior you want or prefer to forbid is up to you.  Be
aware of it, though.  (You may not, in fact, want a default domain.  This
can be accomplished by simply removing the default configuration file from
nginx's site-available directory and removing the symbolic linking from the
Dockerfile.)

### Credit for the Makefile Idea

I got the idea to use a Makefile to encapsulate the
sometimes cumbersome Docker commands from 
[this website](https://nelkinda.com/blog/apache-php-in-docker/#d11e418) and I think it's a
fantastic idea and I wish I'd thought of it.  I'm
new to Docker so maybe there are better ways, but
this sure has made testing this project easy.